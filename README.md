# Ansiblez
Ansible playbooks to perform installation and configuration of an application infrastructure consisting of 
* 2 Apache httpd - Tomcat instances
* 3 ElasticSearch that form a cluster

The application infrastructure is tested against Vagrant Boxes with SLES12. A further Vagrant Box named *tower* may serve
as a control instance where ansible is executed upon.

## Apache Webserver / Tomcat
* Apache Webserver is primarily acting as a reverse proxy to Tomcat forwarding specific requests.
* Tomcat doesn't have any specific configuration rather than providing the runtime for future application deployments.
* _TODO provide a further Apache Webserver VM to act as a Load Balancer which also will handle SSL termination._

## ElasticSearch
* Currently with basic configuration 
  *_TODO Certificates / Keystores / Truststores_
  *_TODO [SearchGuard](https://github.com/floragunncom/search-guard) configuration_
* Convenience scripts are installed on each node via Ansible for
  * Starting `elastic.start`
  * Stopping `elastic.stop`
  * Viewing the output `elastic.log`
